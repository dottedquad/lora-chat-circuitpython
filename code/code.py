# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

#-----------------------
# info
#-----------------------

# Clayton Darwin
# claytondarwin@gmail.com
# https://gitlab.com/duder1966
# https://www.youtube.com/claytondarwin

#99.9% of this code was written by Clayton Darwin.
#I modified some of his code for circuitpython to the best of my ability

import time
import board
import busio
import asyncio
import supervisor
from digitalio import DigitalInOut
import neopixel
import device_rylr9x

#-----------------------
# run options
#-----------------------

def run():
    send() # for mobile ESP32
    #bounce() # for base ESP32


#-----------------------
# message sender object
#-----------------------

# send messages, check responses

class SENDER:

    def __init__(self,network=18,address=1,power=22):

        self.lora = device_rylr9x.RYLR998(network,address,power)

    def loop(self):

        loops = 0
        stack = []

        while 1:

            # notify
            loops += 1
            print('LOOP:',loops)

            # match line in to last sent message
            for line in self.lora.readlines():
                print('  LINE IN:',line)
                if line.startswith('+RCV'):
                    line = self.lora.rcvparse(line)
                    sent = self.lora.sent()
                    recv = [line['addr'],line['dlen'],line['data']]
                    print('    SENT:',sent)
                    print('    RECV:',recv)
                    if sent == recv:
                        print('    MATCH: True')
                        # flash blue LED

            # send new message
            message = 'Hello from loop {}.'.format(loops)
            print('  SEND:',1,message,self.lora.send(1,message))

            # wait a while
            time.sleep(10)


def send():
    s = SENDER()
    s.loop()

#-----------------------
# self run
#-----------------------

run()

#-----------------------
# end
#-----------------------
